<!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="{{url('/')}}"><img src="{{ asset('assets/img/logo.jpeg') }}" alt="" class="img-fluid"></a>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="#header">Accueil</a></li>
          <li><a href="#about">À propos</a></li>
          <li><a href="#services">Objectifs Spécifiques</a></li>
          <li><a href="#portfolio">Réalisations</a></li>
          <li><a href="#blog">Blog</a></li>
          <li class="drop-down"><a href="">Catégories</a>
            <ul>
				@foreach($categories as $category)
					<li><a href="{{ route('category.single', ['id' => $category->id ]) }}">{{ $category->name }}</a></li>
				@endforeach
            </ul>
          </li>
          <li><a href="#contact">Nous contacter</a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->