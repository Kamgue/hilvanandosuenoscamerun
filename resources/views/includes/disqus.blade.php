<div id="disqus_thread"></div>
<script>

/**
*VARIABLES DE CONFIGURATION RECOMMANDÉES: MODIFIEZ ET DÉCOMPOSEZ LA SECTION CI-DESSOUS POUR INSÉRER DES VALEURS DYNAMIQUES DE VOTRE PLATEFORME OU CMS.
* APPRENEZ POURQUOI DÉFINIR CES VARIABLES EST IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

var disqus_config = function () {
this.page.url = "{{ route('post.single',['slug'=>$post->slug]) }}";  // Remplacez PAGE_URL par la variable URL canonique de votre page
this.page.identifier = "post-{{ $post->slug }}"; // Remplacez PAGE_IDENTIFIER par la variable d'identifiant unique de votre page
};

(function() { // NE MODIFIEZ PAS SOUS CETTE LIGNE
var d = document, s = d.createElement('script');
s.src = 'https://smart-laravel-blog.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Veuillez activer JavaScript pour afficher le <a href="https://disqus.com/?ref_noscript">commentaires générés par Disqus.</a></noscript>
                            