 <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <div class="footer-logo">
                  <h2>{{ $settings->site_name }}</h2>
                </div>

                <p>Hilvanando Suenos Camerun est un projet qui s’inscrit dans la coopération internationale pour le développement. Il a été financé par la Diputacion de Huelva à travers l’ONG M-Solidaria dont le siège est dans la commune de Moguer et qui est en partenariat avec la CVX Cameroun.
                  Ce projet vise la formation des jeunes filles mères en couture afin de leur permettre avoir une qualification professionnelle les permettant de pouvoir avoir du travail et de subvenir aux besoins de la famille..</p>
                <div class="footer-icons">
                  <ul>
                    <li>
                      <a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-google"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-pinterest"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- end single footer -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <h4>informations</h4>
                <p>
                  Notre service est à votre disposition.
                </p>
                <div class="footer-contacts">
                  <p><span>Tel:</span> {{ $settings->contact_number }}</p>
                  <p><span>Email:</span> {{ $settings->contact_email }}</p>
                  <p><span>Support en ligne:</span> Du lundi au vendredi 8 heures-18 heures</p>
                </div>
              </div>
            </div>
          </div>
          <!-- end single footer -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
                <h4>Instagram</h4>
                <div class="flicker-img">
                  <a href="#"><img src="{{ asset('assets/img/portfolio/10.jpeg') }}" alt=""></a>
                  <a href="#"><img src="{{ asset('assets/img/portfolio/12.jpeg') }}" alt=""></a>
                  <a href="#"><img src="{{ asset('assets/img/portfolio/13.jpeg') }}" alt=""></a>
                  <a href="#"><img src="{{ asset('assets/img/portfolio/14.jpeg') }}" alt=""></a>
                  <a href="#"><img src="{{ asset('assets/img/portfolio/25.jpeg') }}" alt=""></a>
                  <a href="#"><img src="{{ asset('assets/img/portfolio/36.jpeg') }}" alt=""></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; Copyright <strong> <a href="{{url('/admin/dashboard')}}" class="title">Hilvanando Suenos Camerun</a></strong>. Tous droits réservés
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>