@extends('layouts.app')


@section('content')

     
      @include('admin.includes.errors')

<div class="panel panel-default">
      <div class="panel-heading">
      	Créer un nouveau Tag
      </div>

      <div class="panel-body">
      	<form action="{{ route('tag.store') }}" method="POST">
      		
      		{{ csrf_field() }}

            <div class="form-group">
            	<label for="tag">Tag Nom</label>
            	<input type="text" name="tag" placeholder="Entrez le nom de votre catégorie de blog" class="form-control">
            </div>

            <div class="from-group">
            	 <div class="text-right">
            	 	<button class="btn btn-success" type="submit">Créer une catégorie</button>
            	 </div>
            </div>



      	</form>
      </div>

  </div>

@stop