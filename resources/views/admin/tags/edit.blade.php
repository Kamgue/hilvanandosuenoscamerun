
@extends('layouts.app')


@section('content')

     
      @include('admin.includes.errors')

<div class="panel panel-default">
      <div class="panel-heading">
      	Mis à jour du  Tag :{{ $tag->tag }}
      </div>

      <div class="panel-body">
      	<form action="{{ route('tag.update',['id'=>$tag->id]) }}" method="POST">
      		
      		{{ csrf_field() }}

            <div class="form-group">
            	<label for="tag">Nom du tag</label>
            	<input type="text" name="tag" value="{{ $tag->tag }}"  placeholder="Enter Your Blog Category Name" class="form-control">
            </div>

            <div class="from-group">
            	 <div class="text-right">
            	 	<button class="btn btn-success" type="submit">mise à jour du tag</button>
            	 </div>
            </div>



      	</form>
      </div>

  </div>

@stop