@extends('layouts.app')


@section('content')

     
      @include('admin.includes.errors')

<div class="panel panel-default">
      <div class="panel-heading">
      	Configuration du Site
      </div>

      <div class="panel-body">
      	<form action="{{ route('settings.update') }}" method="POST">
      		
      		{{ csrf_field() }}

            <div class="form-group">
                  <label for="site_name">Nom du site</label>
                  <input type="text" name="site_name" value="{{ $settings->site_name }}" placeholder="Entrez le nom de votre site" class="form-control">
            </div>
            <div class="form-group">
                  <label for="contact_number">Numéro de contact</label>
                  <input type="text" name="contact_number" value="{{ $settings->contact_number }}" placeholder="Entrez votre numéro de contact" class="form-control">
            </div>
            <div class="form-group">
                  <label for="contact_email">Courriel du site</label>
                  <input type="email" name="contact_email" value="{{ $settings->contact_email }}" placeholder="Entrer votre Email" class="form-control">
            </div> 
            <div class="form-group">
            	<label for="address">Adresse</label>
            	<input type="text" name="address" value="{{ $settings->address }}" placeholder="Entrez votre adresse" class="form-control">
            </div>

            <div class="from-group">
            	 <div class="text-right">
            	 	<button class="btn btn-success" type="submit">Configuration des paramètres du site</button>
            	 </div>
            </div>



      	</form>
      </div>

  </div>

@stop