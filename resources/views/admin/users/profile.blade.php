@extends('layouts.app')


@section('content')

     
      @include('admin.includes.errors')

<div class="panel panel-default">
      <div class="panel-heading">
      	Modifier le profil utilisateur
      </div>

      <div class="panel-body">
      <form action="{{ route('user.profile.update') }}" method="POST" enctype="multipart/form-data">
      		
      		{{ csrf_field() }}

            <div class="form-group">
                  <label for="name">Nom d'utilisateur</label>
                  <input type="text" name="name" value="{{ $user->name }}" placeholder="Entrez votre nom d'utilisateur" class="form-control">
            </div>
            <div class="form-group">
                  <label for="email">Courriel de l'utilisateur</label>
                  <input type="email" name="email" value="{{ $user->email }}" placeholder="Entrez votre e-mail d'utilisateur" class="form-control">
            </div>
            <div class="form-group">
                  <label for="password">nouveau mot de passe</label>
                  <input type="password" name="password" placeholder="Entrez votre nouveau mot de passe" class="form-control">
            </div>  
            <div class="form-group">
                  <label for="avatar">Télécharger un nouvel avatar</label>
                  <input type="file" name="avatar" class="form-control">
            </div>
            <div class="form-group">
                  <label for="about">Au propos de vous</label>
                 <textarea class="form-control" name="about" id="about" cols="30" rows="10">@isset($user->profile->about){{ $user->profile->about }}@endisset</textarea>
            </div>
            <div class="form-group">
                  <label for="facebook">Facebook</label>
                  <input type="text" value="{{ $user->facebook }}" name="facebook" placeholder="" class="form-control">
            </div>
             <div class="form-group">
            	<label for="facebook">Youtube</label>
            	<input type="text" value="{{ $user->youtube }}" name="youtube" placeholder="" class="form-control">
            </div>

            <div class="from-group">
            	 <div class="text-right">
            	 	<button class="btn btn-success" type="submit">Mettre à jour le profil</button>
            	 </div>
            </div>



      	</form>
      </div>

  </div>

@stop