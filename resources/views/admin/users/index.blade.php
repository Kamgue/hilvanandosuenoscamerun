@extends('layouts.app')



@section('content')




	 <div class="panel panel-default">
	 	<div class="panel-body">
	 		
			<table class="table table-hover">
		 		<thead>
					<th>Image utilisateur</th>

					<th>Nom d'utilisateur</th>
					<th>Autorisations</th>
					<th>Supprimer</th>

					<tbody>
                       @if($users->count()>0)

						 @foreach($users as $user)

		              <tr>
		              	<td>
		              		<img style="width: 60px;height: 60px;border-radius: 50%;" src="{{ asset(optional($user->profile)->avatar) }}" alt="">
		              	</td>
		              	  <td>
		              	  	{{ $user->name }}
		              	  </td>
		              	  <td>
		              	  	    @if($user->admin)
		              	  	    <a href="{{ route('user.not_admin', ['id'=>$user->id]) }}" class="btn btn-xs btn-danger">Supprimer l'administrateur</a>
 
		              	  	    @else

		              	  	    <a href="{{ route('user.admin', ['id'=>$user->id]) }}" class="btn btn-xs btn-success">Faire administrateur</a>
  
		              	  	    @endif
		              	  </td>
		              	  
		              	  	<td>
                              @if(Auth::id() !== $user->id)
                                    <a href="{{ route('user.delete', ['id' => $user->id]) }}" class="btn btn-xs btn-danger">Supprimer</a>
                              @endif
                              </td>

		              	  	
		              	 

		              	

		              </tr>
		             

					 @endforeach

	                  @else

	                  <tr>
	                  	<th colspan="5" style="background-color: rgb(23,45,67);color: white;" class="text-center">Post Create Not yet</th>
	                  </tr>

                          @endif

					</tbody>
				</thead>
			</table>
	 	</div>
	 </div>
	

@stop