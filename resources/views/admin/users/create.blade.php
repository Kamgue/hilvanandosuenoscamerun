@extends('layouts.app')


@section('content')

     
      @include('admin.includes.errors')

<div class="panel panel-default">
      <div class="panel-heading">
      	Créer un nouvel utilisateur
      </div>

      <div class="panel-body">
      	<form action="{{ route('user.store') }}" method="POST">
      		
      		{{ csrf_field() }}

            <div class="form-group">
                  <label for="name">Nom d'utilisateur</label>
                  <input type="text" name="name" placeholder="Entrez votre nom d'utilisateur" class="form-control">
            </div>
            <div class="form-group">
            	<label for="email">Courriel de l'utilisateur</label>
            	<input type="email" name="email" placeholder="Entrez votre e-mail d'utilisateur" class="form-control">
            </div>

            <div class="from-group">
            	 <div class="text-right">
            	 	<button class="btn btn-success" type="submit">Créer un utilisateur</button>
            	 </div>
            </div>



      	</form>
      </div>

  </div>

@stop