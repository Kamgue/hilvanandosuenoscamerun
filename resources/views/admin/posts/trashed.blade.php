@extends('layouts.app')



@section('content')




	 <div class="panel panel-default">
	 	<div class="panel-body">
	 		
			<table class="table table-hover">
		 		<thead>
					<th>Post l'image</th>

					<th>Post Titre</th>
					<th>Éditer</th>
					<th>Restorer</th> 
					<th>Supprimer</th>

					<tbody>

						@if($posts->count()>0)
                            @foreach($posts as $post)

			              <tr>
			              	<td>
			              		<img style="width: 90px;height: 50px;" src="{{ $post->featured}}" alt="{{$post->title}}">
			              	</td>

			              	<td>
			              	      {{ $post->title }}
			              	</td>
			              	 <td>Editer</td>
			              	 <td><a class="btn btn-xs btn-success" href="{{ route('posts.restore', ['id'=>$post->id]) }}">Restorer</a>
			              	 </td>
			              	 <td><a class="btn btn-xs btn-danger" href="{{ route('post.kill', ['id'=>$post->id]) }}">Supprimer</a>
			              	 </td>

			              </tr>
			             

						   @endforeach

						@else

                                  <tr>
                                  	<th colspan="5" style="background-color: rgb(23,45,67);color: white;" class="text-center">Pas de corbeille ici</th>
                                  </tr>

						@endif


						 
					</tbody>
				</thead>
			</table>
	 	</div>
	 </div>
	

@stop