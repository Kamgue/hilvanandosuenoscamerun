<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('uploads/avatars/logo.png') }}" rel="icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'FomaCorp') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">

     @yield('styles')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Basculer la navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'FomaCorp') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">S'identifier</a></li>
                            <li><a href="{{ route('register') }}">S'inscrire</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Se déconnecter
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

         <div class="container">
             <div class="row">

                @if(auth::check())

                 <div class="col-lg-4">
                     <ul class="list-group">

                        <li class="list-group-item">
                            <a href="{{ route('home') }}">Accueil</a>
                        </li>

                         <li class="list-group-item">
                             <a href="{{ route('post.create') }}">Créer un nouveau post</a>
                         </li>
                         <li class="list-group-item">
                             <a href="{{ route('category.create') }}">Créer une nouvelle catégorie</a>
                         </li>
                         <li class="list-group-item">
                             <a href="{{ route('categories') }}">Toutes catégories</a>
                         </li>
                         <li class="list-group-item">
                             <a href="{{ route('tag.create') }}">Créer un nouveau tags</a>
                         </li>
                         <li class="list-group-item">
                             <a href="{{ route('tags') }}">Tous les tags</a>
                         </li> 
                    
                         <li class="list-group-item">
                             <a href="{{ route('posts') }}">Tous les posts</a>
                         </li>
                        <li class="list-group-item">
                             <a href="{{ route('posts.trashed') }}">Tous les posts supprimés</a>
                         </li>

                         @if(Auth::user()->admin)
                         <li class="list-group-item">
                             <a href="{{ route('users') }}">Utilisateurs</a>
                         </li>
                         <li class="list-group-item">
                             <a href="{{ route('user.create') }}">Créer un nouvel utilisateur</a>
                         </li>
						 
						 <li class="list-group-item">
                             <a href="{{ url('https://dc03-webmail.237rs.cc') }}" target="_blank">Web mail fomacorp</a>
                         </li>

                          <li class="list-group-item">
                             <a href="{{ url('https://mail.google.com') }}" target="_blank">Gmail de google</a>
                         </li>
						 
						 <li class="list-group-item">
                             <a href="{{ url('https://analytics.google.com') }}" target="_blank">Analytics de google</a>
                         </li>

                         @endif

                          <li class="list-group-item">
                             <a href="{{ route('user.profile') }}">Mon profil</a>
                         </li>


                         @if(Auth::user()->admin)

                          <li class="list-group-item">
                             <a href="{{ route('settings') }}">Paramètres du blog</a>
                         </li>
                         @endif

                     </ul>
                 </div>

                 @endif

                 <div class="col-lg-8">

                       
                     @yield('content')
                 </div>
             </div>
         </div>

        
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>

     <script>
          @if(Session::has('success'))
                toastr.success("{{ Session::get('success') }}")
          @endif
          @if(Session::has('info'))
                toastr.info("{{ Session::get('info') }}")
          @endif
     </script>

     @yield('scripts')
</body>
</html>
