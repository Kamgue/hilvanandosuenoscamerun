<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ $title }} - Résultats de recherche: {{ $query }}</title>
  <meta content="Entreprise digitale au Cameroun" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('uploads/avatars/logo.png') }}" rel="icon">
  <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163137465-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163137465-2');
</script>

</head>

<body data-spy="scroll" data-target="#navbar-example">
	
	  @include('includes.header')
	  
	  <br>
	  <br>
	  <div class="blog-page area-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="page-head-blog">
              <div class="single-blog-page">
                <!-- search option start -->
                <form method="GET" action="{{url('/results')}}">
                  <div class="search-option">
                    <input name="query" placeholder="Tapez et appuyez sur Entrée ..." type="text">
                    <button class="button" type="submit">
                      <i class="fa fa-search"></i>
                    </button>
                  </div>
                </form>
                <!-- search option end -->
              </div>
              <div class="single-blog-page">
                <!-- recent start -->
                <div class="left-blog">
                  <h4>Résultats: {{ $query }}</h4>
                </div>
                <!-- recent end -->
              </div>
              <div class="single-blog-page">
                <div class="left-blog">
                  <h4>categories</h4>
                  <ul>
                        @foreach($categories as $category)
							<a href="{{ route('category.single', ['id' => $category->id ]) }}"><li>{{ $category->name }}</li></a>
						@endforeach
                   </ul>
                </div>
              </div>
              <div class="single-blog-page">
                <div class="left-tags blog-tags">
                  <div class="popular-tag left-side-tags left-blog">
                    <h4> tags populaires </h4>
                    <ul>
					@if($posts->count() > 0)
					@foreach($posts as $post)
					  @foreach($post->tags as $tag)
					  <li>
                        <a href="{{ route('tag.single',['id'=>$tag->id]) }}" class="w-tags-item">{{$tag->tag}}</a>
                      </li>
                      @endforeach
					@endforeach
					@endif
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- End left sidebar -->
          <!-- Start single blog -->
          <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
				
				@if($posts->count() > 0)
				@foreach($posts as $post)
                <div class="single-blog">
                  <div class="single-blog-img">
                    <a href="{{ route('post.single', ['slug' => $post->slug ]) }}"">
                      <img src="{{ $post->featured }}" alt="image" width="100%" style="height:500px;">
                    </a>
                  </div>
                  <div class="blog-meta">
                    <span class="comments-type">
                      <i class="fa fa-home"></i>
                      <a href="{{ route('category.single', ['id' => $post->category->id ]) }}">{{ $post->category->name }}</a>
                    </span>
                    <span class="date-type">
                      <i class="fa fa-calendar"></i>{{ $post->created_at->toFormattedDateString() }}
                    </span>
                  </div>
                  <div class="blog-text">
                    <h4>
                      <a href="{{ route('post.single', ['slug' => $post->slug ]) }}">{{ $post->title }}</a>
                    </h4>
                    <p>
                      {{ $post->title }}
                    </p>
                  </div>
                  <span>
                    <a href="{{ route('post.single', ['slug' => $post->slug ]) }}" class="ready-btn">Lire la suite</a>
                  </span>
                </div>
				@endforeach
				@else
						<br><br>
                          <div class="comments-type"">
                            <div class="date-type">
                                <h2 class="blog-text" >Le résultat ne correspond pas</h2>
                                <br>
                                
                               <h1><a href="{{ route('index') }}" class="blog-text" >Retourner à notre page d'accueil</a></h1> 
                            </div>
                        </div>

				@endif
              </div>
              <!-- End single blog -->
              <div class="blog-pagination">
                <ul class="pagination">
                  <li><a href="#">&lt;</a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&gt;</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Blog Page -->

  </main><!-- End #main -->

<!-- ======= Footer ======= -->
  @include('includes.footer')
 <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader##"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('assets/vendor/appear/jquery.appear.js') }}"></script>
  <script src="{{ asset('assets/vendor/knob/jquery.knob.js') }}"></script>
  <script src="{{ asset('assets/vendor/parallax/parallax.js') }}"></script>
  <script src="{{ asset('assets/vendor/wow/wow.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/nivo-slider/js/jquery.nivo.slider.js') }}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/venobox/venobox.min.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>