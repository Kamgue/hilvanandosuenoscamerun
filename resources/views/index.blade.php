<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ $title }} - Hilvanando Suenos Camerun</title>
  <meta content="Entreprise digitale au Cameroun" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('uploads/avatars/logo.png') }}" rel="icon">
  <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
  <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163137465-2"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-163137465-2');
	</script>

</head>

<body data-spy="scroll" data-target="#navbar-example">
	
	@include('includes.header')

    @include('includes.slide')
  <main id="main">

    <!-- ======= About Section ======= -->
    <div id="about" class="about-area area-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2>À propos de Hilvanando Suenos Camerun</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <!-- single-well start-->
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="well-left">
              <div class="single-well">
                <a href="#">
                  <img src="{{ asset('assets/img/about/1.png') }}" alt="">
                </a>
              </div>
            </div>
          </div>
          <!-- single-well end-->
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="well-middle">
              <div class="single-well">
                <a href="#">
                  <h4 class="sec-head">“Hilvanando Suenos Camerun”</h4>
                </a>
                <p>
                  Hilvanando Suenos Camerun est un projet qui s’inscrit dans la coopération internationale pour le développement. Il a été financé par la Diputacion de Huelva à travers l’ONG M-Solidaria dont le siège est dans la commune de Moguer et qui est en partenariat avec la CVX Cameroun.
                  Ce projet vise la formation des jeunes filles mères en couture afin de leur permettre avoir une qualification professionnelle les permettant de pouvoir avoir du travail et de subvenir aux besoins de la famille.
                  Commencé en février de 2023 avec 15 filles-mères, il a pris fin en fin février 2024 à la satisfaction de toutes les personnes impliquées dans ledit projet.
                  Arrivé à la fin de ce projet, nos remerciements vont à l’endroit de la Diputacion de Huelva dont l’appui financier a permis la réalisation de ce projet. Nous remercions de tout cœur l’ONG M-Solidaria qui a porté ce projet auprès de la Diputación de Huelva. Un merci spécial va à l’endroit de sa présidente Roció. Nous remercions également les enseignants et toutes les personnes qui n’ont ménagé aucun effort pour que ce projet puisse se réaliser à merveille comme nous l’avons constaté.
                  Hilvanando Suenos Camerun a fait rêver 15 jeunes filles mères ainsi que toute leur famille. Des lueurs d’espoirs se lit sur leur visage dans leur robe endimanché confectionnée par elles-mêmes. 
                  
                  
                </p>
                <ul>
                  <li>
                    <i class="fa fa-check"></i> Au mois d’octobre 2024 nous Hilvanandosuenoscamerun a reçu la visite du vice-président de l’ONG M-Solidaria.;
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <!-- End col-->
        </div>
      </div>
    </div><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    <div id="services" class="services-area area-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline services-head text-center">
              <h2>Objectifs spécifiques</h2>
			  <p>
				Afin de remplir sa mission et réaliser sa vision selon ses valeurs dans le strict respect des lois, Hilvanando Suenos Camerun est créée pour réaliser les activités suivantes.
			  </p>
            </div>
          </div>
        </div>
        <div class="row text-center">
          <!-- Start Left services -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon" href="#">
                    <i class="fa fa-tasks"></i>
                  </a>
                  <h4>Autonomie économique</h4>
                  <p>
                    Objectif: Accroître les opportunités d'emploi et d'entrepreneuriat pour les jeunes filles mères formées en couture.
                    Indicateurs: Nombre de jeunes filles mères ayant trouvé un emploi ou créé leur propre entreprise de couture dans les 6 mois suivant la fin de la formation.                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon" href="#">
                    <i class="fa fa-pencil"></i>
                  </a>
                  <h4>Niveau de vie amélioré</h4>
                  <p>
                    Objectif: Augmenter les revenus des jeunes filles mères formées en couture.
                    Indicateurs: Augmentation moyenne du revenu mensuel des jeunes filles mères formées en couture dans les 12 mois suivant la fin de la formation.                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <!-- end col-md-4 -->
            <div class=" about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon" href="#">
                    <i class="fa fa-cogs"></i>
                  </a>
                  <h4>Pauvreté réduite</h4>
                  <p>
                    Objectif: Diminuer le nombre de jeunes filles mères vivant en dessous du seuil de pauvreté dans les 24 mois suivant la fin de la formation.
                    Indicateurs: Pourcentage de jeunes filles mères formées en couture vivant en dessous du seuil de pauvreté dans les 24 mois suivant la fin de la formation.                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <!-- end col-md-4 -->
            <div class=" about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon" href="#">
                    <i class="fa fa-comments"></i>
                  </a>
                  <h4>Égalité des chances.</h4>
                  <p>
                    Objectif: Accroître la participation des jeunes filles mères formées en couture à la vie communautaire et économique.
                    Indicateurs: Nombre de jeunes filles mères formées en couture participant à des activités communautaires ou économiques dans les 12 mois suivant la fin de la formation.                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <!-- End Left services -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <!-- end col-md-4 -->
            <div class=" about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon" href="#">
                    <i class="fa fa-leaf"></i>
                  </a>
                  <h4> Compétences renforcées </h4>
                  <p>
                    Objectif: Améliorer les compétences techniques et entrepreneuriales des jeunes filles mères formées en couture.
                    Indicateurs: Niveau de satisfaction des jeunes filles mères formées en couture quant à la qualité de la formation reçue.                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
          <!-- End Left services -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <!-- end col-md-4 -->
            <div class=" about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon" href="#">
                    <i class="fa fa-users"></i>
                  </a>
                  <h4> Pérennité du projet </h4>
                  <p>
                    Objectif: Mobiliser des ressources financières supplémentaires pour assurer la continuité du projet et la formation de nouvelles jeunes filles mères.
                    Indicateurs: Montant des fonds supplémentaires mobilisés pour le projet Hilvanando Suenos Camerun dans les 12 mois suivant la fin de la formation des 15 jeunes filles mères.                  </p>
                </div>
              </div>
              <!-- end about-details -->
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Services Section -->

    <!-- ======= Skills Section ======= -->
    <div class="our-skill-area fix hidden-sm">
      <div class="test-overly"></div>
      <div class="skill-bg area-padding-2">
        <div class="container">
          <!-- section-heading end -->
          <div class="row">
            <!-- single-skill start -->
            <div class="col-xs-12 col-sm-3 col-md-3 text-center">
              <div class="single-skill">
				<a href="#" target="_blank">
                <div class="progress-circular">
                  <input type="text" class="knob" value="0" data-rel="75" data-linecap="round" data-width="175" data-bgcolor="#fff" data-fgcolor="#3EC1D5" data-thickness=".20" data-readonly="true" disabled>
                  <h3 class="progress-h4">Formation en couture pour les jeunes filles mères</h3>
                </div>
				</a>
              </div>
            </div>
            <!-- single-skill end -->
            <!-- single-skill start -->
            <div class="col-xs-12 col-sm-3 col-md-3 text-center">
			<a href="#" target="_blank">
              <div class="single-skill">
                <div class="progress-circular">
                  <input type="text" class="knob" value="0" data-rel="75" data-linecap="round" data-width="175" data-bgcolor="#fff" data-fgcolor="#3EC1D5" data-thickness=".20" data-readonly="true" disabled>
                  <h3 class="progress-h4">Appui à l'entrepreneuriat des jeunes filles mères formées</h3>
                </div>
              </div>
			</a>
            </div>
            <!-- 'single-skill end -->
            <!-- single-skill start -->
            <div class="col-xs-12 col-sm-3 col-md-3 text-center">
			<a href="#" target="_blank">
              <div class="single-skill">
                <div class="progress-circular">
                  <input type="text" class="knob" value="0" data-rel="75" data-linecap="round" data-width="175" data-bgcolor="#fff" data-fgcolor="#3EC1D5" data-thickness=".20" data-readonly="true" disabled>
                  <h3 class="progress-h4">Sensibilisation à la situation des jeunes filles mères </h3>
                </div>
              </div>
			</a>
            </div>
            <!-- single-skill end -->
            <!-- single-skill start -->
            <div class="col-xs-12 col-sm-3 col-md-3 text-center">
			<a href="#" target="_blank">
              <div class="single-skill">
                <div class="progress-circular">
                  <input type="text" class="knob" value="0" data-rel="100" data-linecap="round" data-width="175" data-bgcolor="#fff" data-fgcolor="#3EC1D5" data-thickness=".20" data-readonly="true" disabled>
                  <h3 class="progress-h4">Renforcement des capacités des organisations locales  </h3>
                </div>
              </div>
			 </a>
            </div>
            <!-- single-skill end -->
          </div>
        </div>
      </div>
    </div><!-- End Skills Section -->

    <!-- ======= Team Section ======= -->
    <div id="team" class="our-team-area area-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h3>Autonomiser les jeunes filles mères camerounaises par la formation en couture et l'appui à l'entrepreneuriat, afin d'améliorer leur niveau de vie et de réduire la pauvreté. </h3>
            </div>
          </div>
        </div>
      </div>
    </div><!--' End Team Section -->

    <!-- ======= Rviews Section ======= -->
    <div class="reviews-area hidden-xs">
      <div class="work-us">
        <div class="work-left-text">
          <a href="#">
            <img src="{{ asset('assets/img/about/c.jpg') }}" alt="">
          </a>
        </div>
        <div class="work-right-text text-center">
          <h2>Les besoins à long et à court terme </h2>
          <h5>Ces projets, à la fois à court et à long terme, visent à autonomiser les jeunes filles mères camerounaises et à leur offrir un avenir meilleur. </h5>
          <a href="#contact" class="ready-btn scrollto">Nous contacter</a>
        </div>
      </div>
    </div><!-- End Rviews Section -->

    <!-- ======= Portfolio Section ======= -->
    <div id="portfolio" class="portfolio-area area-padding fix">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2>Nos réalisations</h2>
            </div>
          </div>
        </div>
        <div class="row wesome-project-1 fix">
          <!-- Start Portfolio -page -->
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="awesome-menu ">
              <ul class="project-menu">
                <li>
                  <a href="#" class="active" data-filter="*">Toute</a>
                </li>
                <li>
                  <a href="#" data-filter=".development">Projet couture</a>
                </li>
                <li>
                  <a href="#" data-filter=".design">Autres</a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="row awesome-project-content">
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 design development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="{{ asset('assets/img/portfolio/1.jpeg') }}" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="{{ asset('assets/img/portfolio/1.jpeg') }}">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 photo">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="{{ asset('assets/img/portfolio/2.jpeg') }}" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="{{ asset('assets/img/portfolio/2.jpeg') }}">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 design">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="{{ asset('assets/img/portfolio/3.jpeg') }}" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="{{ asset('assets/img/portfolio/3.jpeg') }}">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 photo development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="{{ asset('assets/img/portfolio/4.jpeg') }}" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="{{ asset('assets/img/portfolio/4.jpeg') }}">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- single-awesome-project end -->
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 development">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="{{ asset('assets/img/portfolio/5.jpeg') }}" alt="" /></a>
                <div class="add-actions text-center text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="{{ asset('assets/img/portfolio/5.jpeg') }}">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
            // Loop through image numbers 6 to 61
            for ($i = 6; $i <= 61; $i++) {
              $imageName = $i . '.jpeg';
            ?>

            <div class="col-md-4 col-sm-4 col-xs-12 design photo">
              <div class="single-awesome-project">
                <div class="awesome-img">
                  <a href="#"><img src="{{ asset('assets/img/portfolio/' . $imageName) }}" alt="" /></a>
                  <div class="add-actions text-center">
                    <div class="project-dec">
                      <a class="venobox" data-gall="myGallery" href="{{ asset('assets/img/portfolio/' . $imageName) }}">
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <?php
            }
            ?>

        </div>
      </div>
    </div><!-- End Portfolio Section -->

    <!-- ======= Blog Section ======= -->
    <div id="blog" class="blog-area">
      <div class="blog-inner area-padding">
        <div class="blog-overly"></div>
        <div class="container ">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="section-headline text-center">
                <h2>Dernières nouvelles</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <!-- Start Left Blog -->
			@isset ($posts[0])
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="single-blog">
                <div class="single-blog-img">
                  <a href="{{ route('post.single', ['slug' => $posts[0]->slug ]) }}">
                    <img src="{{ $posts[0]->featured }}" style="width:600px; height:600px;" alt="{{ $posts[0]->title }}">
                  </a>
                </div>
                <div class="blog-meta">
                  <span class="comments-type">
                    <i class="fa fa-home"></i>
                    <a href="{{ route('category.single', ['id' => $posts[0]->category->id ]) }}">{{ $posts[0]->category->name }}</a>
                  </span>
                  <span class="date-type">
                    <i class="fa fa-calendar"></i>{{ $posts[0]->created_at->toFormattedDateString() }}
                  </span>
                </div>
                <div class="blog-text">
                  <h4>
                    <a href="{{ route('post.single', ['slug' => $posts[0]->slug ]) }}">{{ $posts[0]->title }}</a>
                  </h4>
                </div>
                <span>
                  <a href="{{ route('post.single', ['slug' => $posts[0]->slug ]) }}" class="ready-btn">Lire la suite</a>
                </span>
              </div>
              <!-- Start single blog -->
            </div>
			@endisset
			
			@isset ($posts[1])
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="single-blog">
                <div class="single-blog-img">
                  <a href="{{ route('post.single', ['slug' => $posts[1]->slug ]) }}">
                    <img src="{{ $posts[1]->featured }}" style="width:400px; height:600px;" alt="{{ $posts[1]->title }}">
                  </a>
                </div>
                <div class="blog-meta">
                  <span class="comments-type">
                    <i class="fa fa-home"></i>
                    <a href="{{ route('category.single', ['id' => $posts[1]->category->id ]) }}">{{ $posts[1]->category->name }}</a>
                  </span>
                  <span class="date-type">
                    <i class="fa fa-calendar"></i>{{ $posts[1]->created_at->toFormattedDateString() }}
                  </span>
                </div>
                <div class="blog-text">
                  <h4>
                    <a href="{{ route('post.single', ['slug' => $posts[1]->slug ]) }}">{{ $posts[1]->title }}</a>
                  </h4>
                </div>
                <span>
                  <a href="{{ route('post.single', ['slug' => $posts[1]->slug ]) }}" class="ready-btn">Lire la suite</a>
                </span>
              </div>
              <!-- Start single blog -->
            </div>
			@endisset
			
			@isset ($posts[2])
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="single-blog">
                <div class="single-blog-img">
                  <a href="{{ route('post.single', ['slug' => $posts[2]->slug ]) }}">
                    <img src="{{ $posts[2]->featured }}"  style="width:600px; height:600px;" alt="{{ $posts[2]->title }}">
                  </a>
                </div>
                <div class="blog-meta">
                  <span class="comments-type">
                    <i class="fa fa-home"></i>
                    <a href="{{ route('category.single', ['id' => $posts[2]->category->id ]) }}">{{ $posts[2]->category->name }}</a>
                  </span>
                  <span class="date-type">
                    <i class="fa fa-calendar"></i>{{ $posts[2]->created_at->toFormattedDateString() }}
                  </span>
                </div>
                <div class="blog-text">
                  <h4>
                    <a href="{{ route('post.single', ['slug' => $posts[2]->slug ]) }}">{{ $posts[2]->title }}</a>
                  </h4>
                </div>
                <span>
                  <a href="{{ route('post.single', ['slug' => $posts[2]->slug ]) }}" class="ready-btn">Lire la suite</a>
                </span>
              </div>
              <!-- Start single blog -->
            </div>
			@endisset
            <!-- End Right Blog-->
          </div>
        </div>
      </div>
    </div><!-- End Blog Section -->

    <!-- ======= Suscribe Section ======= -->
    <div class="suscribe-area">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs=12">
            <div class="suscribe-text text-center">
              <h3>Bienvenue à HSC: www.hilvanandosuenoscamerun.com</h3>
              <a class="sus-btn" href="www.hilvanandosuenoscamerun.com">Recevez nos actualités</a>
            </div>
          </div>
        </div>
      </div>
    </div><!-- End Suscribe Section -->

    <!-- ======= Contact Section ======= -->
    <div id="contact" class="contact-area">
      <div class="contact-inner area-padding">
        <div class="contact-overly"></div>
        <div class="container ">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="section-headline text-center">
                <h2>Nous contacter</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <!-- Start contact icon column -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="contact-icon text-center">
                <div class="single-icon">
                  <i class="fa fa-mobile"></i>
                  <p>
                    Téléphone: {{ $settings->contact_number }}<br>
                    <span>Du lundi au vendredi 8 heures-18 heures</span>
                  </p>
                </div>
              </div>
            </div>
            <!-- Start contact icon column -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="contact-icon text-center">
                <div class="single-icon">
                  <i class="fa fa-envelope-o"></i>
                  <p>
                    Email: {{ $settings->contact_email }}<br>
                    <span>Site web: www.hilvanandosuenoscamerun.com</span>
                  </p>
                </div>
              </div>
            </div>
            <!-- Start contact icon column -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="contact-icon text-center">
                <div class="single-icon">
                  <i class="fa fa-map-marker"></i>
                  <p>
                    Boite postale :  S/C PB 758 Bafoussam <br>
                    <span>{{ $settings->address }}</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="row">

            <!-- Start Google Map -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <!-- Start Map -->
              <iframe src="{{ asset('assets/img/about/2.jpg') }}" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
              <!-- End Map -->
            </div>
            <!-- End Google Map -->

            <!-- Start  contact -->
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form contact-form">
                <form action="mail" method="post" role="form">
				{{csrf_field()}}
                  <div class="form-group">
                    <input type="text" required name="nom" class="form-control" id="nom" placeholder="votre nom" data-rule="minlen:4" data-msg="Veuillez saisir au moins 4 caractères" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <input type="email" required class="form-control" name="email" id="email" placeholder="Votre email" data-rule="email" data-msg="Veuillez saisir un e-mail valide" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <input type="text" required class="form-control" name="subject" id="subject" placeholder="Objet" data-rule="minlen:4" data-msg="Veuillez saisir au moins 8 caractères du sujet" />
                    <div class="validate"></div>
                  </div>
                  <div class="form-group">
                    <textarea required class="form-control" name="message" rows="5" data-rule="required" data-msg="S'il vous plaît écrivez quelque chose pour nous" placeholder="Message"></textarea>
                    <div class="validate"></div>
                  </div>
                  <div class="mb-3">
                    <div class="loading">Chargement</div>
                    <div class="error-message"></div>
                    <div class="sent-message">Votre message a été envoyé. Je vous remercie!</div>
                  </div>
                  <div class="text-center"><button class="btn btn-success" style="background-color:#006cce;" type="submit">Envoyer le message</button></div>
                </form>
              </div>
            </div>
            <!-- End Left contact -->
          </div>
        </div>
      </div>
    </div><!-- End Contact Section -->

  </main><!-- End #main -->
  <!-- ======= Footer ======= -->
  @include('includes.footer')
 <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader##"></div>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('assets/vendor/appear/jquery.appear.js') }}"></script>
  <script src="{{ asset('assets/vendor/knob/jquery.knob.js') }}"></script>
  <script src="{{ asset('assets/vendor/parallax/parallax.js') }}"></script>
  <script src="{{ asset('assets/vendor/wow/wow.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/nivo-slider/js/jquery.nivo.slider.js') }}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/venobox/venobox.min.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js') }}"></script>

</body>

</html>