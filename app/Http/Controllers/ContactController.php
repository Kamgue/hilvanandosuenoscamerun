<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
 
class ContactController extends Controller
{
 
    public function store(Request $request)
    {
        Mail::to('fomacorp000@gmail.com')
            ->send(new Contact($request->except('_token')));
 
        return redirect()->to('/');
    }
}