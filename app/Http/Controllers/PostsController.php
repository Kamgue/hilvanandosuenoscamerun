<?php

namespace App\Http\Controllers;
use Session;
use Auth;
use App\Tag;
use App\Category;
use App\Post;
use DB;


use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.posts.index')->with('posts', Post::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        if($categories->count() == 0)
        {
            Session::flash('info', 'Vous devez avoir choisi au moins une catégorie');

            return redirect()->back();
        }
        return view('admin.posts.create')->with('categories',$categories)
                                         ->with('tags',Tag::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	DB::beginTransaction();
        $this->validate($request,[

            'title' => 'required',
            'featured' => 'required|image',
            'body' => 'required',
            'category_id'=> 'required',
             'tags'=>'required'
            

        ]);

        $featured = $request->featured;
        $featuerd_new = time().$featured->getClientoriginalName();
        $featured->move('uploads/posts', $featuerd_new);

        $post = Post::create([


                'title' => $request->title,
                'body' => $request->body,
                'featured' => 'uploads/posts/'. $featuerd_new,
                'category_id' => $request->category_id,
                'slug'=> str_slug($request->title),
                'user_id'=>Auth::id()


        ]);

          $post->tags()->attach($request->tags);
          DB::commit();

       Session::flash('success','Votre message a été créé avec succès');

       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Post::find($id);

        return view('admin.posts.edit')->with('posts',$posts)

                                       ->with('categories', Category::all())
                                       ->with('tags',Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    	DB::beginTransaction();
         $this->validate($request,[

            'title' => 'required',
            'body' => 'required',
            'category_id'=> 'required'
            

        ]);

        $posts = Post::find($id);

        if($request->hasfile('featured')){


        $featured = $request->featured;
        $featuerd_new = time().$featured->getClientoriginalName();
        $featured->move('uploads/posts', $featuerd_new);
        $posts->featured ='uploads/posts/'.$featuerd_new;

        }


         $posts->title = $request->title;
         $posts->body = $request->body;
         $posts->category_id = $request->category_id;
         $posts->save();
         $posts->tags()->sync($request->tags);

      	DB::commit();
       Session::flash('success', 'Vous avez mis à jour un article avec succès.');
        return redirect()->route('posts');
       



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posts = Post::find($id);
        $posts->delete();
        Session::flash('success', 'Vous avez supprimé avec succès un message.');
        return redirect()->back();
    }

    public function trashed()
    {
 
       $posts = Post::onlyTrashed()->get();
      

       return view('admin.posts.trashed')->with('posts', $posts);

    }

    public function kill($id)
    {
       $posts = Post::withTrashed()->where('id',$id)->first();
       $posts->forceDelete();
       Session::flash('success', 'Vous avez réussi à supprimer définitivement un message.');
       return redirect()->back();
      
    }

    public function restore($id)

    {
        $posts = Post::withTrashed()->where('id',$id)->first();
        $posts->restore();
        Session::flash('success', 'Vous avez réussi à restaurer un message.');
        return redirect()->route('posts');


    }
}
