<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Profile;
use DB;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.users.profile')->with('user', Auth::user());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        

        $this->validate($request,[

           'name' => 'required',
           'email'=> 'required|email',
           'facebook' => 'nullable',
           'youtube' => 'nullable'



        ]);

        $user = User::find(Auth::user()->id);


          if($request->hasfile('avatar')){


            $avatar = $request->avatar;
            $avatar_new = time().$avatar->getClientOriginalName();
            $avatar->move('uploads/avatars',$avatar_new);
            $user->profile->avatar ='uploads/avatars/' . $avatar_new;
            $user->profile->save();
          }

          $user->name = $request->name;
          $user->email = $request->email;
		  $profile=$user->profile;
		  if($request->facebook!=""){
			$profile->facebook = $request->facebook;
		  }
		  if($request->youtube!=""){
			$profile->youtube = $request->youtube;
		  }
          $profile->about = $request->about;
         

         
          $profile->save();
			
		  if($request->password!=""){
			  if($request->has('password')){
				$user->password = bcrypt($request->password);
			  }
		  }

           $user->save();

           Session::flash('success','Profile Updated!');
           return redirect()->back();
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
