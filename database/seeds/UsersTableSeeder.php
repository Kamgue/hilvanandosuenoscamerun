<?php

use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user =  App\User::create([
              'name' => 'hsc',
              'email' => 'hsc@gmail.com',
              'password'=>bcrypt('hsc'),
              'admin' =>1 

        ]);


        App\Profile::create([


           'user_id' => $user->id,
           'avatar' => 'uploads/avatars/1.png',
          'about' => 'Développeur de la plateforme merci de me contacter en cas de problème',
           'facebook' => 'facebook.com',
           'youtube' => 'youtube.com'

        ]);
    }
}
