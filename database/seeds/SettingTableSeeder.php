<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([

           'site_name' =>"HSC",
           'contact_number' =>'237697012104',
           'contact_email' =>'hsc@gmail.com',
           'address' =>'Cameroun'


        ]);
    }
}
